package dam.androidjacob.u2p4conversor;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.text.DecimalFormat;

public class MainActivity extends LogActivity {

    private String DEBUG_TAG = "LOG--";
    private String mensajeTXError;
    TextView txError;


    public MainActivity() {
        super.setDEBUG_TAG(this.getClass().getSimpleName());
        DEBUG_TAG += this.getClass().getSimpleName();
    }



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        txError = findViewById(R.id.textViewError);
        setUI();
    }

    //TODO Ex3 Añadir logs
    //Añado todas los metodos que controlan el ciclo de vida junto con el extends de la clase LogActivity
    @Override
    protected void onRestart() {
        super.onRestart();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }


    //TODO Ex4 correccion desaparicion textView
    //Añado el onRestore y el onSaveInstance para que al girar la pantalla no se elimine el textView
    //Guardo el valor en el bundle que luego se le pasara al onRestore para que pueda hacer el setText
    @Override
    protected void onRestoreInstanceState(@NonNull Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        mensajeTXError = savedInstanceState.getString("txError");
        txError.setText(mensajeTXError);
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        mensajeTXError = txError.getText().toString();
        outState.putString("txError",mensajeTXError);
    }



    private void setUI(){
        EditText etPulgada = findViewById(R.id.et_Pulgada);
        EditText etResultado = findViewById(R.id.et_Resultado);
        Button buttonConvertirCM = findViewById(R.id.button_ConvertirCM);



        //TODO Ex2 añadir txErro
        //Añadido el if para comprobar si el numero es menor a 1 y lanzar la excepcion y cambiar el texto al mensage del error
        buttonConvertirCM.setOnClickListener(
                view -> {
                    try {
                        if(Integer.valueOf(String.valueOf(etPulgada.getText())) < 1){
                             throw new Exception("Solo numeros >= 1");
                        }else{
                            etResultado.setText(convertirCM(etPulgada.getText().toString()));
                            Log.i(DEBUG_TAG,"Boton convertir a CM pulsado");
                            txError.setText("");
                            mensajeTXError = "";
                        }

                    }catch (Exception e){
                        Log.e("LogsConversor", e.getMessage());
                        txError.setText(e.getMessage());
                }
                });

        //TODO Ex1 doble cambio cm <=> pulgadas y Ex2 añadir txError
        //Añadido un boton nuevo para poder cambiar entre cm y pulgadas ademas de todo lo explicado en el comentario anterior sobre el otro boton

        Button buttonConvertirPulgadas = findViewById(R.id.button_ConvertirPulgadas);

        buttonConvertirPulgadas.setOnClickListener(view -> {
            try {
                if(Integer.valueOf(String.valueOf(etPulgada.getText())) < 1){
                    throw new Exception("Solo numeros >= 1");
                }else{
                    etResultado.setText(convertirPulgadas(etPulgada.getText().toString()));
                    Log.i(DEBUG_TAG,"Boton convertir a Pulgadas pulsado");
                    txError.setText("");
                    mensajeTXError = "";
                }
            }catch (Exception e){
                Log.e("LogConversor", e.getMessage());
                txError.setText(e.getMessage());
            }
        });
    }

    //TODO Ex1 doble cambio cm <=> pulgadas
    //Añadido los 2 metodos para poder hacer el calculo
    private String convertirCM(String pulgadaText){
        DecimalFormat formato = new DecimalFormat("#.00");
        double pulgadaValue = Double.parseDouble(pulgadaText) * 2.54;
        return String.valueOf(formato.format(pulgadaValue));
    }

    private String convertirPulgadas(String pulgadaText){
        DecimalFormat formato = new DecimalFormat("#.00");
        double pulgadaValue = Double.parseDouble(pulgadaText) / 2.54;
        return String.valueOf(formato.format(pulgadaValue));
    }
}